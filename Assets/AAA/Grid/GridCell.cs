using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GridCell : MonoBehaviour
{
	public float Height => transform.lossyScale.z;
	public float Width => transform.lossyScale.x;

	public TextMeshProUGUI CellText;

	public bool isWalkable;

	public MeshRenderer MeshRenderer;

}
