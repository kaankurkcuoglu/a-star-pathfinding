using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Grid : MonoBehaviour
{
	public int Height;
	public int Width;

	public GridCell CellPrefab;

	private GridCell[] GridCells;

	[Range(0, 1)] public float NonwalkableCellRatio;

	public Material NonWalkableMaterial;
	public Material WalkableMaterial;


	private void Awake()
	{
		CreateGrid();
		// SetRandomNonWalkableCells();
	}

	private void SetRandomNonWalkableCells()
	{
		foreach (var gridCell in GridCells)
		{
			var walkable = Random.Range(0f, 1f) > NonwalkableCellRatio;
			gridCell.isWalkable = walkable;
			gridCell.MeshRenderer.material = walkable ? WalkableMaterial : NonWalkableMaterial;
		}
	}

	private GridCell CreateGridCell(int x, int y)
	{
		var instantiatedCell = Instantiate(CellPrefab, transform, true);

		instantiatedCell.transform.position = new Vector3(x * instantiatedCell.Width, 0, y * instantiatedCell.Height);

		instantiatedCell.CellText.text = $"{x}\n{y}";

		return instantiatedCell;
	}


	private void CreateGrid()
	{
		var cellSize = Height * Width;

		GridCells = new GridCell[cellSize];

		for (int x = 0, i = 0; x < Width; x++)
		{
			for (int y = 0; y < Height; y++, i++)
			{
				GridCells[i] = CreateGridCell(x, y);
			}
		}
	}
}